from server import create_app

client = create_app().test_client()

def test_health_check():
    response = client.get("/")
    assert response.status_code == 200
    assert response.get_json()["status"] == "ok"

test_payload = {
    "input":[0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0]
}
def test_prediction():
    response = client.post("/predict", json=test_payload)
    assert response.status_code == 200
    assert type(response.get_json()["prediction"]) is str
    # Specific for this payload
    assert response.get_json()["prediction"] == "[0]"

# Simple dockerfile example
FROM python:3.8.10-slim
WORKDIR /usr/src/app
COPY . .
RUN pip3 install -r requirements.txt
EXPOSE 5000
# Need to change user for security reasons...
CMD ["python3","server.py"]
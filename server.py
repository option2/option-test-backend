from flask import Flask, jsonify, request
import joblib
import datetime as dt

# App running commands and selection for TDD approach
def create_app():
    app = Flask(__name__)

    # Mini cache redis-like, best approach if prediction is not available or takes longer to process
    cache_pred = {}

    # Initialize model variable for correct load checking
    model = joblib.load('models/pickle_model.pkl')

    # Smoke Test 1: SRE Health check status for automation
    @app.route("/")
    def health_check():
        return jsonify({"status": "ok"})

    # Main route for asking predictions
    @app.route('/predict', methods=['POST'])
    def predict():
        req_data = request.get_json(force=True)["input"]
        # Checking cache data
        if str(req_data) in cache_pred.keys():
            return cache_pred[(str(req_data))]
        start = dt.datetime.now()
        # Smoke Test 2: Model validation for health status
        if not model: 
            return jsonify({"model": "Not available"})
        # Getting data payload from request
        # Data type checks
        if req_data is None or type(req_data) is not list:
            return jsonify({"model": "Invalid input"})
        # Prediction using model
        prediction = model.predict([req_data])
        end = dt.datetime.now()
        # Response payload includes prediction, process init time and the time it takes to process the prediction
        response = jsonify({'time': str(end), 'time_elapsed':str((end-start).total_seconds()) + ' seconds', 'prediction': str(prediction)})
        # Cache saving data
        cache_pred[str(req_data)] = response
        return response
    return app

if __name__ == '__main__':
    create_app().run(host='0.0.0.0',port=5000)
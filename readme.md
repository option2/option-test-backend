Hi, I'm Rolando Valenzuela and I've been working on this repository for several days, I'll try to explain each of the steps in terms of how each stage of the challenge developed.

## Development and Git
- First I had to develop the app, looking for the most feasible and fastest way to deploy it, I tend to the use of Flask as a framework and others that allowed me to handle the pickle file in the best way. I also used the TDD standard, running basic tests with pytest (testing directory) as well as reliability using smoke test (server file) to ensure that the service works correctly when deployed. The model file is included statically, an improvement here may be that it is dynamic and downloads it when the kubernetes service is implemented, so it can be stored in a bucket/S3 also is a better flow of versions working with the models developers.
- On the other hand, I decided to use Gitlab as my Git alternative since it is more plug and play for the CI/CD issue and this allows me to focus on the deployment issue.

## Infrastructure as Code and Kubernetes
- I had the intention of deploying the docker image in Cloud Run but there were problems with my Google account (billing failed, apparently), and in the end I decided to switch to using AWS and with it the selected platform to get the best environment of adaptability and reliability was by using a small kubernetes cluster on EKS.
- As for terraform, it is made up of the 4 basic files, which represent the versioning of the providers, in this case AWS, the variables include the region where the service will be hosted, output is empty but available if necessary, and in main.tf it is divided by construction of the vpc elements, availability zones, and the EKS cluster.
- The Kubernetes cluster requires Gitlab keys and vice versa for its correct operation and they are mapped to various variables such as:
	- `AWS_ACCESS_KEY_ID` 
	- `AWS_SECRET_ACCESS_KEY` 
	- `AWS_DEFAULT_REGION` 

That allow access to the cli of the cluster, and on the other hand, Kubernetes needs to deploy a Secret to be able to access the Gitlab container registry (Here it is better to pass the repository to ECR, so as not to perform a security key exchange).

- I put the terraform repository aside to have a better order in terms of deployment and to be able to give the infrastructure its own pipeline and versioning. Repository: [Terraform](https://gitlab.com/option2/option-test-terraform)
- As mentioned above the service was hosted on EKS, a shared configuration file was used for deployment and the service, and exposed with the following command `kubectl expose deployment api --type=LoadBalancer --name=api` to be publicly accessible via the following URL `a0279d68c796c4fffbe520275961bb98-544949702.us-west-2.elb.amazonaws.com`. **I will be pending if it changes**
- Health Check Route: **GET** `a0279d68c796c4fffbe520275961bb98-544949702.us-west-2.elb.amazonaws.com/` (port 80)
- Predict Route: **POST** `a0279d68c796c4fffbe520275961bb98-544949702.us-west-2.elb.amazonaws.com/predict` (port 80)
	- Example Payload: `{"input":[0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0]}`
	- We can improve this input by interpreting all the variables and extending the function to convert the raw data to something that the Regression Model could receive.
	- Example Response: `{
"prediction":  "[0]",
"time":  "2023-03-07 11:51:39.282491",
"time_elapsed":  "0.000322 seconds"
}`
	- The prediction response could be converted to the real meaning of the topic, and it was converted to string for better usability in JSON, but for what I got of the jupyter-notebook it does mean some delay in the airplane with the `input` variables.

## Metrics
The tests were carried out with the wrk tool, using the following parameters:
- wrk.method = "POST" 
- wrk.headers["Authorization"] = "Bearer <token>"
- wrk.headers["content-type"] = "application/json"
- wrk.body = "{ \"input\": [0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0]}"
- wrk -t5 -c55 -d45 --latency -s wrk/post.lua

Results:
- Local:

![](images/local.png)

- Kubernetes (Low Price/Performance Mode):

![](images/low_performance_kubernetes.png)

- Kubernetes (Vertical Adjustment) No significant improvement due EKS hardware limitation, but increases a little bit:

![](images/vertical.png)

- Kubernetes (Horizontal Adjustment) It supports more connections, not linear increments but good performance ratio:

![](images/horizontal.png)

## Q&A
- How could you improve the performance of the previous tests?
	- There are three paths to follow, the first is to increase the computing and memory capacity, or vertical growth, this is demonstrated with respect to my local test that is limited in resources at the time of being elastic, secondly we could consider in horizontal growth since for the first test the cluster had only one active service, being able to replicate it and divide the load. Finally, there is the possibility of improving the code or changing the framework, among the possibilities is FastAPI, which tends to be faster than Flask, and also the issue of a more advanced cache could be used in the case of many repeated requests.
- What would be the ideal mechanisms so that only authorized systems can access this API?
	- When using platforms such as GCP or AWS, there is the issue of services such as IAM or Firewalls at the low levels that allow us to indicate if any resource or host is the right one to be able to make a request, also at the code level it is necessary to implement various mechanisms such as the of a login and the bearer token that serve to carry out a more important filtering at the application level. Also, I think it is important to use code composition analysis tools to identify vulnerabilities or bugs and mitigate them properly. Also there are so many protocols and good practices about least privileges or how to harden the apps.
- Does this mechanism add any level of latency to the consumer? Why?
	- Yes, since it always involves an extra process such as middleware, processes such as validation, authentication, licensing or some encryption method are usually expensive for data processing. Although this should already be implicit in the loss of performance of the app, since it is essential for the protection of data integrity.
- What would be the SLIs and SLOs that you would define and why?
	- When we talk about a web service, most of the time people tend to despair if there is a service that is very slow or has many errors when processing the information. For this reason, I believe that the SLIs that should be included in most cases for issues of active monitoring of a platform are the response time or latency, the number of requests supported, the recovery time from a crash or the time to develop some fix. While for SLOs it would be the target measure for this, ideally being improvable over time, where I take as an example the average response times of the platform with the low power cluster which was around 150ms, the goal could be to look for Alternatives to lower it to 120ms or increase the capacity of supported requests before those 150ms become seconds and affect the user experience. I think the idea of SLOs should go hand in hand with the SLAs determined by end users.

## Next Steps:
- Consolidate Helm to update correctly the running versions
- ArgoCD Implementation
- Model bucket Implementation
